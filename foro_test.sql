-- MySQL dump 10.13  Distrib 8.0.22, for Linux (x86_64)
--
-- Host: localhost    Database: foro
-- ------------------------------------------------------
-- Server version	8.0.22-0ubuntu0.20.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_group` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_group_permissions` (
  `id` int NOT NULL AUTO_INCREMENT,
  `group_id` int NOT NULL,
  `permission_id` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_permission` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add permission',1,'add_permission'),(2,'Can change permission',1,'change_permission'),(3,'Can delete permission',1,'delete_permission'),(4,'Can view permission',1,'view_permission'),(5,'Can add group',2,'add_group'),(6,'Can change group',2,'change_group'),(7,'Can delete group',2,'delete_group'),(8,'Can view group',2,'view_group'),(9,'Can add user',3,'add_user'),(10,'Can change user',3,'change_user'),(11,'Can delete user',3,'delete_user'),(12,'Can view user',3,'view_user'),(13,'Can add content type',4,'add_contenttype'),(14,'Can change content type',4,'change_contenttype'),(15,'Can delete content type',4,'delete_contenttype'),(16,'Can view content type',4,'view_contenttype'),(17,'Can add session',5,'add_session'),(18,'Can change session',5,'change_session'),(19,'Can delete session',5,'delete_session'),(20,'Can view session',5,'view_session'),(21,'Can add site',6,'add_site'),(22,'Can change site',6,'change_site'),(23,'Can delete site',6,'delete_site'),(24,'Can view site',6,'view_site'),(25,'Can add log entry',7,'add_logentry'),(26,'Can change log entry',7,'change_logentry'),(27,'Can delete log entry',7,'delete_logentry'),(28,'Can view log entry',7,'view_logentry'),(29,'Can add Forum',8,'add_forum'),(30,'Can change Forum',8,'change_forum'),(31,'Can delete Forum',8,'delete_forum'),(32,'Can view Forum',8,'view_forum'),(33,'Can add Post',9,'add_post'),(34,'Can change Post',9,'change_post'),(35,'Can delete Post',9,'delete_post'),(36,'Can view Post',9,'view_post'),(37,'Can add Topic',10,'add_topic'),(38,'Can change Topic',10,'change_topic'),(39,'Can delete Topic',10,'delete_topic'),(40,'Can view Topic',10,'view_topic'),(41,'Can add Attachment',11,'add_attachment'),(42,'Can change Attachment',11,'change_attachment'),(43,'Can delete Attachment',11,'delete_attachment'),(44,'Can view Attachment',11,'view_attachment'),(45,'Can add Topic poll',12,'add_topicpoll'),(46,'Can change Topic poll',12,'change_topicpoll'),(47,'Can delete Topic poll',12,'delete_topicpoll'),(48,'Can view Topic poll',12,'view_topicpoll'),(49,'Can add Topic poll option',13,'add_topicpolloption'),(50,'Can change Topic poll option',13,'change_topicpolloption'),(51,'Can delete Topic poll option',13,'delete_topicpolloption'),(52,'Can view Topic poll option',13,'view_topicpolloption'),(53,'Can add Topic poll vote',14,'add_topicpollvote'),(54,'Can change Topic poll vote',14,'change_topicpollvote'),(55,'Can delete Topic poll vote',14,'delete_topicpollvote'),(56,'Can view Topic poll vote',14,'view_topicpollvote'),(57,'Can add Forum track',15,'add_forumreadtrack'),(58,'Can change Forum track',15,'change_forumreadtrack'),(59,'Can delete Forum track',15,'delete_forumreadtrack'),(60,'Can view Forum track',15,'view_forumreadtrack'),(61,'Can add Topic track',16,'add_topicreadtrack'),(62,'Can change Topic track',16,'change_topicreadtrack'),(63,'Can delete Topic track',16,'delete_topicreadtrack'),(64,'Can view Topic track',16,'view_topicreadtrack'),(65,'Can add Forum profile',17,'add_forumprofile'),(66,'Can change Forum profile',17,'change_forumprofile'),(67,'Can delete Forum profile',17,'delete_forumprofile'),(68,'Can view Forum profile',17,'view_forumprofile'),(69,'Can add Forum permission',18,'add_forumpermission'),(70,'Can change Forum permission',18,'change_forumpermission'),(71,'Can delete Forum permission',18,'delete_forumpermission'),(72,'Can view Forum permission',18,'view_forumpermission'),(73,'Can add Group forum permission',19,'add_groupforumpermission'),(74,'Can change Group forum permission',19,'change_groupforumpermission'),(75,'Can delete Group forum permission',19,'delete_groupforumpermission'),(76,'Can view Group forum permission',19,'view_groupforumpermission'),(77,'Can add User forum permission',20,'add_userforumpermission'),(78,'Can change User forum permission',20,'change_userforumpermission'),(79,'Can delete User forum permission',20,'delete_userforumpermission'),(80,'Can view User forum permission',20,'view_userforumpermission');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(150) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'pbkdf2_sha256$216000$H03CpaCMReaD$vVbnY1LDXvscZ0GnNK6YVGe5enVA5shMdr8FVsy7BqM=','2020-11-28 07:28:43.425169',1,'root','','','masizo.geek@gmail.com',1,1,'2020-11-28 07:26:57.815019'),(2,'pbkdf2_sha256$216000$HMWFg1RrH9Bi$Rh1bcz5NwpDyKMsHnKp65MGgWGpKO0qvQ5KWovJshls=','2020-11-29 03:41:47.339598',0,'luis','','','luis@m.com',0,1,'2020-11-29 02:34:10.361629'),(3,'pbkdf2_sha256$216000$tGRaSyGNJDrM$sihk1SCvBNYpaA41AmXZSKPug0P8DsiYpk/HRMphUEw=','2020-11-30 04:46:53.002673',1,'seclab','','','seclab@mail.com',1,1,'2020-11-29 03:17:06.872278'),(4,'pbkdf2_sha256$216000$6BHXL9TlX2ju$dpPNRT/eGaaZuZIlCKrLcLxl6hvTBy79nw/BXAKRsPo=','2020-11-29 20:27:58.799618',0,'ale','','','',0,1,'2020-11-29 03:53:31.879616');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_user_groups` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `group_id` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `permission_id` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `django_admin_log` (
  `id` int NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int DEFAULT NULL,
  `user_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`),
  CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `django_admin_log_chk_1` CHECK ((`action_flag` >= 0))
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2020-11-28 07:38:29.773171','1','Ansiedad',1,'[{\"added\": {}}]',8,1),(2,'2020-11-28 07:43:41.711438','1','Ansiedad',2,'[{\"changed\": {\"fields\": [\"Description\", \"Forum image\"]}}]',8,1),(3,'2020-11-28 07:43:47.617106','1','Ansiedad',2,'[{\"changed\": {\"fields\": [\"Description\", \"Forum image\"]}}]',8,1),(4,'2020-11-28 07:45:27.102829','1','Ansiedad',2,'[{\"changed\": {\"fields\": [\"Description\", \"Forum image\"]}}]',8,1),(5,'2020-11-28 07:45:31.244369','1','Ansiedad',2,'[{\"changed\": {\"fields\": [\"Description\"]}}]',8,1),(6,'2020-11-28 07:46:29.343495','2','Salud mental',1,'[{\"added\": {}}]',8,1),(7,'2020-11-28 07:46:40.357920','1','Ansiedad',2,'[{\"changed\": {\"fields\": [\"Parent\", \"Description\"]}}]',8,1),(8,'2020-11-28 07:47:19.910745','3','Depresión',1,'[{\"added\": {}}]',8,1),(9,'2020-11-28 07:47:35.669774','4','Pensamientos suicidas',1,'[{\"added\": {}}]',8,1),(10,'2020-11-28 07:47:48.553670','5','Transtorno bipolar',1,'[{\"added\": {}}]',8,1),(11,'2020-11-28 07:47:59.258762','6','Deficit de atención',1,'[{\"added\": {}}]',8,1),(12,'2020-11-28 07:48:16.031418','7','Esquizofrenia',1,'[{\"added\": {}}]',8,1),(13,'2020-11-28 07:48:27.225579','8','Anorexia',1,'[{\"added\": {}}]',8,1),(14,'2020-11-28 07:48:39.591262','9','Anorexia',1,'[{\"added\": {}}]',8,1),(15,'2020-11-28 07:49:07.802753','10','Bulimia',1,'[{\"added\": {}}]',8,1),(16,'2020-11-28 07:49:47.956757','11','Trastorno obsesivo-compulsivo',1,'[{\"added\": {}}]',8,1),(17,'2020-11-28 07:50:18.723823','12','Trastorno de pánico',1,'[{\"added\": {}}]',8,1),(18,'2020-11-28 07:50:49.780734','13','Insomnio',1,'[{\"added\": {}}]',8,1),(19,'2020-11-28 07:51:03.634363','14','Autoestima',1,'[{\"added\": {}}]',8,1);
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `django_content_type` (
  `id` int NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (7,'admin','logentry'),(2,'auth','group'),(1,'auth','permission'),(3,'auth','user'),(4,'contenttypes','contenttype'),(8,'forum','forum'),(11,'forum_attachments','attachment'),(9,'forum_conversation','post'),(10,'forum_conversation','topic'),(17,'forum_member','forumprofile'),(18,'forum_permission','forumpermission'),(19,'forum_permission','groupforumpermission'),(20,'forum_permission','userforumpermission'),(12,'forum_polls','topicpoll'),(13,'forum_polls','topicpolloption'),(14,'forum_polls','topicpollvote'),(15,'forum_tracking','forumreadtrack'),(16,'forum_tracking','topicreadtrack'),(5,'sessions','session'),(6,'sites','site');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `django_migrations` (
  `id` int NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2020-11-28 06:26:39.730579'),(2,'auth','0001_initial','2020-11-28 06:26:43.785184'),(3,'admin','0001_initial','2020-11-28 06:26:58.293290'),(4,'admin','0002_logentry_remove_auto_add','2020-11-28 06:27:01.854767'),(5,'admin','0003_logentry_add_action_flag_choices','2020-11-28 06:27:02.223567'),(6,'contenttypes','0002_remove_content_type_name','2020-11-28 06:27:05.314401'),(7,'auth','0002_alter_permission_name_max_length','2020-11-28 06:27:06.983878'),(8,'auth','0003_alter_user_email_max_length','2020-11-28 06:27:08.744990'),(9,'auth','0004_alter_user_username_opts','2020-11-28 06:27:08.912457'),(10,'auth','0005_alter_user_last_login_null','2020-11-28 06:27:10.504733'),(11,'auth','0006_require_contenttypes_0002','2020-11-28 06:27:10.599942'),(12,'auth','0007_alter_validators_add_error_messages','2020-11-28 06:27:10.704963'),(13,'auth','0008_alter_user_username_max_length','2020-11-28 06:27:12.426924'),(14,'auth','0009_alter_user_last_name_max_length','2020-11-28 06:27:14.404148'),(15,'auth','0010_alter_group_name_max_length','2020-11-28 06:27:16.902963'),(16,'auth','0011_update_proxy_permissions','2020-11-28 06:27:17.261238'),(17,'auth','0012_alter_user_first_name_max_length','2020-11-28 06:27:19.555747'),(18,'forum','0001_initial','2020-11-28 06:27:20.607099'),(19,'forum_conversation','0001_initial','2020-11-28 06:27:35.767096'),(20,'forum_conversation','0002_post_anonymous_key','2020-11-28 06:27:48.174355'),(21,'forum_conversation','0003_auto_20160228_2051','2020-11-28 06:27:48.319169'),(22,'forum_conversation','0004_auto_20160427_0502','2020-11-28 06:27:53.683588'),(23,'forum_conversation','0005_auto_20160607_0455','2020-11-28 06:27:54.873163'),(24,'forum_conversation','0006_post_enable_signature','2020-11-28 06:27:55.764439'),(25,'forum_conversation','0007_auto_20160903_0450','2020-11-28 06:28:02.290228'),(26,'forum_conversation','0008_auto_20160903_0512','2020-11-28 06:28:02.465130'),(27,'forum_conversation','0009_auto_20160925_2126','2020-11-28 06:28:02.580888'),(28,'forum_conversation','0010_auto_20170120_0224','2020-11-28 06:28:07.198981'),(29,'forum','0002_auto_20150725_0512','2020-11-28 06:28:07.291740'),(30,'forum','0003_remove_forum_is_active','2020-11-28 06:28:10.221364'),(31,'forum','0004_auto_20170504_2108','2020-11-28 06:28:17.087399'),(32,'forum','0005_auto_20170504_2113','2020-11-28 06:28:17.273344'),(33,'forum','0006_auto_20170523_2036','2020-11-28 06:28:22.761835'),(34,'forum','0007_auto_20170523_2140','2020-11-28 06:28:22.912893'),(35,'forum','0008_forum_last_post','2020-11-28 06:28:26.521228'),(36,'forum','0009_auto_20170928_2327','2020-11-28 06:28:26.653195'),(37,'forum','0010_auto_20181103_1401','2020-11-28 06:28:26.811845'),(38,'forum','0011_auto_20190627_2132','2020-11-28 06:28:28.584249'),(39,'forum_attachments','0001_initial','2020-11-28 06:28:29.611904'),(40,'forum_attachments','0002_auto_20181103_1404','2020-11-28 06:28:32.371902'),(41,'forum_conversation','0011_remove_post_poster_ip','2020-11-28 06:28:35.979752'),(42,'forum_conversation','0012_auto_20200423_1049','2020-11-28 06:28:36.302739'),(43,'forum_member','0001_initial','2020-11-28 06:28:37.635059'),(44,'forum_member','0002_auto_20160225_0515','2020-11-28 06:28:40.105056'),(45,'forum_member','0003_auto_20160227_2122','2020-11-28 06:28:40.251135'),(46,'forum_member','0004_auto_20181103_1406','2020-11-28 06:28:40.370886'),(47,'forum_member','0005_auto_20200423_1049','2020-11-28 06:28:40.500038'),(48,'forum_permission','0001_initial','2020-11-28 06:28:44.182667'),(49,'forum_permission','0002_auto_20160607_0500','2020-11-28 06:28:59.598157'),(50,'forum_permission','0003_remove_forumpermission_name','2020-11-28 06:29:01.424288'),(51,'forum_permission','0004_auto_20190319_2240','2020-11-28 06:29:04.449696'),(52,'forum_permission','0005_userforumpermission_authenticated_user','2020-11-28 06:29:05.277405'),(53,'forum_polls','0001_initial','2020-11-28 06:29:08.716616'),(54,'forum_polls','0002_auto_20151105_0029','2020-11-28 06:29:19.677982'),(55,'forum_tracking','0001_initial','2020-11-28 06:29:22.530590'),(56,'forum_tracking','0002_auto_20160607_0502','2020-11-28 06:29:31.444703'),(57,'sessions','0001_initial','2020-11-28 06:29:32.309642'),(58,'sites','0001_initial','2020-11-28 06:29:33.619375'),(59,'sites','0002_alter_domain_unique','2020-11-28 06:29:34.637237');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_expire_date_a5c62663` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('69cp28kbvdiokcyzv0ws2hj9xtk7rpij','eyJfYW5vbnltb3VzX2ZvcnVtX2tleSI6ImFkZjBmZGU5YWZjODRiYWFhMzNkNDJkMDI4NjgxNmIzIn0:1kiudi:58dm0CY2Z3tV6xFjLWhQVbDvisc07JIykcgMRX-3rT8','2020-12-12 07:27:26.911119'),('d1rsaqi8yh4zg1xbsf1xr86hisn2mvig','eyJfYW5vbnltb3VzX2ZvcnVtX2tleSI6ImRkNjY0N2JkNzI2OTQwMmRiMzhhZGMyZWY2NjA2NTIzIn0:1kjCXn:1DIF7UfIj5Wrnx2h6wnhp-hGSUzfYYW-4VXnslWI0Ao','2020-12-13 02:34:31.183634'),('ls3wujyrq1run74sjwk1fx6xafptqlu8','eyJfYW5vbnltb3VzX2ZvcnVtX2tleSI6ImRjMjE2ZTU3MTgxOTQ2MjViMGY1ODc5YzFiYTljMmQzIn0:1kiudi:XOGFdIllecuBY1W7-Qy1QFI6j9vH0xRwdrtkGxlBy2o','2020-12-12 07:27:26.349626'),('mxyacjwh670vq7mgrbhauyaeu0n9yb9q','eyJfYW5vbnltb3VzX2ZvcnVtX2tleSI6IjQyNTFmNzYyZmQyZTRmNjk5YWUyODMzY2ExN2NjYTJhIn0:1kiudi:erM4IlQBbWwZrXkx7HwcO_BB7QWrTUFd2ptgMCph6wA','2020-12-12 07:27:26.473664'),('oajxke6yb7q3n07ltyazvdrjec2pjxl2','.eJxVjM0OgyAQhN-Fc0MUAbHH3vsMZNldqrVC4s_BNH33YuLF0yQz3zdf4SHltE95W3zM8zb5kXdxFw2H1pDDYGPQCBUAaFfF0jI4FxtxK-a29n5bePYDHcq1C4Ajp2OgN6RXlpjTOg9BHog810U-M_HncbKXgx6WvtiBSHcarFXBUa0ap8nV7Epyq4y20MXOKIyRKRJUCC1ZJMMKDSEjit8fPBlMeQ:1kjb5R:7smZLKt_QksBIz2DDqLcRO3dJFygAWlq3t_AMVDgBAg','2020-12-14 04:46:53.072714');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_site`
--

DROP TABLE IF EXISTS `django_site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `django_site` (
  `id` int NOT NULL AUTO_INCREMENT,
  `domain` varchar(100) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_site_domain_a2e37b91_uniq` (`domain`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_site`
--

LOCK TABLES `django_site` WRITE;
/*!40000 ALTER TABLE `django_site` DISABLE KEYS */;
INSERT INTO `django_site` VALUES (1,'example.com','example.com');
/*!40000 ALTER TABLE `django_site` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `forum_attachments_attachment`
--

DROP TABLE IF EXISTS `forum_attachments_attachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `forum_attachments_attachment` (
  `id` int NOT NULL AUTO_INCREMENT,
  `file` varchar(100) NOT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `post_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `forum_attachments_at_post_id_0476a843_fk_forum_con` (`post_id`),
  CONSTRAINT `forum_attachments_at_post_id_0476a843_fk_forum_con` FOREIGN KEY (`post_id`) REFERENCES `forum_conversation_post` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forum_attachments_attachment`
--

LOCK TABLES `forum_attachments_attachment` WRITE;
/*!40000 ALTER TABLE `forum_attachments_attachment` DISABLE KEYS */;
/*!40000 ALTER TABLE `forum_attachments_attachment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `forum_conversation_post`
--

DROP TABLE IF EXISTS `forum_conversation_post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `forum_conversation_post` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created` datetime(6) NOT NULL,
  `updated` datetime(6) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `content` longtext NOT NULL,
  `username` varchar(155) DEFAULT NULL,
  `approved` tinyint(1) NOT NULL,
  `update_reason` varchar(255) DEFAULT NULL,
  `updates_count` int unsigned NOT NULL,
  `_content_rendered` longtext,
  `poster_id` int DEFAULT NULL,
  `topic_id` int NOT NULL,
  `updated_by_id` int DEFAULT NULL,
  `anonymous_key` varchar(100) DEFAULT NULL,
  `enable_signature` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `forum_conversation_p_topic_id_f6aaa418_fk_forum_con` (`topic_id`),
  KEY `forum_conversation_post_updated_by_id_86093355_fk_auth_user_id` (`updated_by_id`),
  KEY `forum_conversation_post_poster_id_19c4e995_fk_auth_user_id` (`poster_id`),
  KEY `forum_conversation_post_approved_a1090910` (`approved`),
  KEY `forum_conversation_post_enable_signature_b1ce8b55` (`enable_signature`),
  CONSTRAINT `forum_conversation_p_topic_id_f6aaa418_fk_forum_con` FOREIGN KEY (`topic_id`) REFERENCES `forum_conversation_topic` (`id`),
  CONSTRAINT `forum_conversation_post_poster_id_19c4e995_fk_auth_user_id` FOREIGN KEY (`poster_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `forum_conversation_post_updated_by_id_86093355_fk_auth_user_id` FOREIGN KEY (`updated_by_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `forum_conversation_post_chk_1` CHECK ((`updates_count` >= 0))
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forum_conversation_post`
--

LOCK TABLES `forum_conversation_post` WRITE;
/*!40000 ALTER TABLE `forum_conversation_post` DISABLE KEYS */;
INSERT INTO `forum_conversation_post` VALUES (1,'2020-11-28 07:44:18.830064','2020-11-29 03:23:45.432811','Test','Hey',NULL,1,NULL,1,'<p>Hey</p>',1,1,3,NULL,1),(2,'2020-11-29 03:24:05.999397','2020-11-29 03:24:05.999511','Re: Test','Hola',NULL,1,NULL,0,'<p>Hola</p>',3,1,NULL,NULL,1),(3,'2020-11-29 03:53:55.696961','2020-11-30 04:45:50.288612','Re: Test','Soy Ale',NULL,1,NULL,1,'<p>Soy Ale</p>',4,1,4,NULL,1);
/*!40000 ALTER TABLE `forum_conversation_post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `forum_conversation_topic`
--

DROP TABLE IF EXISTS `forum_conversation_topic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `forum_conversation_topic` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created` datetime(6) NOT NULL,
  `updated` datetime(6) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `type` smallint unsigned NOT NULL,
  `status` int unsigned NOT NULL,
  `approved` tinyint(1) NOT NULL,
  `posts_count` int unsigned NOT NULL,
  `views_count` int unsigned NOT NULL,
  `last_post_on` datetime(6) DEFAULT NULL,
  `forum_id` int NOT NULL,
  `poster_id` int DEFAULT NULL,
  `first_post_id` int DEFAULT NULL,
  `last_post_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `forum_conversation_topic_forum_id_e9cfe592_fk_forum_forum_id` (`forum_id`),
  KEY `forum_conversation_topic_poster_id_0dd4fa07_fk_auth_user_id` (`poster_id`),
  KEY `forum_conversation_topic_slug_c74ce2cc` (`slug`),
  KEY `forum_conversation_topic_type_92971eb5` (`type`),
  KEY `forum_conversation_topic_status_e78d0ae4` (`status`),
  KEY `forum_conversation_topic_approved_ad3fcbf9` (`approved`),
  KEY `forum_conversation_t_last_post_id_e14396a2_fk_forum_con` (`last_post_id`),
  KEY `forum_conversation_t_first_post_id_ca473bd1_fk_forum_con` (`first_post_id`),
  CONSTRAINT `forum_conversation_t_first_post_id_ca473bd1_fk_forum_con` FOREIGN KEY (`first_post_id`) REFERENCES `forum_conversation_post` (`id`),
  CONSTRAINT `forum_conversation_t_last_post_id_e14396a2_fk_forum_con` FOREIGN KEY (`last_post_id`) REFERENCES `forum_conversation_post` (`id`),
  CONSTRAINT `forum_conversation_topic_forum_id_e9cfe592_fk_forum_forum_id` FOREIGN KEY (`forum_id`) REFERENCES `forum_forum` (`id`),
  CONSTRAINT `forum_conversation_topic_poster_id_0dd4fa07_fk_auth_user_id` FOREIGN KEY (`poster_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `forum_conversation_topic_chk_1` CHECK ((`type` >= 0)),
  CONSTRAINT `forum_conversation_topic_chk_2` CHECK ((`status` >= 0)),
  CONSTRAINT `forum_conversation_topic_chk_3` CHECK ((`posts_count` >= 0)),
  CONSTRAINT `forum_conversation_topic_chk_4` CHECK ((`views_count` >= 0))
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forum_conversation_topic`
--

LOCK TABLES `forum_conversation_topic` WRITE;
/*!40000 ALTER TABLE `forum_conversation_topic` DISABLE KEYS */;
INSERT INTO `forum_conversation_topic` VALUES (1,'2020-11-28 07:44:18.384235','2020-11-30 04:45:50.374580','Test','test',0,0,1,3,13,'2020-11-29 03:53:55.696961',1,1,1,3);
/*!40000 ALTER TABLE `forum_conversation_topic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `forum_conversation_topic_subscribers`
--

DROP TABLE IF EXISTS `forum_conversation_topic_subscribers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `forum_conversation_topic_subscribers` (
  `id` int NOT NULL AUTO_INCREMENT,
  `topic_id` int NOT NULL,
  `user_id` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `forum_conversation_topic_topic_id_user_id_b2c961d5_uniq` (`topic_id`,`user_id`),
  KEY `forum_conversation_t_user_id_7e386a79_fk_auth_user` (`user_id`),
  CONSTRAINT `forum_conversation_t_topic_id_34ebca87_fk_forum_con` FOREIGN KEY (`topic_id`) REFERENCES `forum_conversation_topic` (`id`),
  CONSTRAINT `forum_conversation_t_user_id_7e386a79_fk_auth_user` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forum_conversation_topic_subscribers`
--

LOCK TABLES `forum_conversation_topic_subscribers` WRITE;
/*!40000 ALTER TABLE `forum_conversation_topic_subscribers` DISABLE KEYS */;
INSERT INTO `forum_conversation_topic_subscribers` VALUES (1,1,4);
/*!40000 ALTER TABLE `forum_conversation_topic_subscribers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `forum_forum`
--

DROP TABLE IF EXISTS `forum_forum`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `forum_forum` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created` datetime(6) NOT NULL,
  `updated` datetime(6) NOT NULL,
  `name` varchar(100) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `description` longtext,
  `image` varchar(100) DEFAULT NULL,
  `link` varchar(200) DEFAULT NULL,
  `link_redirects` tinyint(1) NOT NULL,
  `type` smallint unsigned NOT NULL,
  `link_redirects_count` int unsigned NOT NULL,
  `last_post_on` datetime(6) DEFAULT NULL,
  `display_sub_forum_list` tinyint(1) NOT NULL,
  `_description_rendered` longtext,
  `lft` int unsigned NOT NULL,
  `rght` int unsigned NOT NULL,
  `tree_id` int unsigned NOT NULL,
  `level` int unsigned NOT NULL,
  `parent_id` int DEFAULT NULL,
  `direct_posts_count` int unsigned NOT NULL,
  `direct_topics_count` int unsigned NOT NULL,
  `last_post_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `forum_forum_slug_b9acc50d` (`slug`),
  KEY `forum_forum_type_30239563` (`type`),
  KEY `forum_forum_tree_id_84a9227d` (`tree_id`),
  KEY `forum_forum_parent_id_22edea05` (`parent_id`),
  KEY `forum_forum_last_post_id_81b59e37_fk_forum_conversation_post_id` (`last_post_id`),
  CONSTRAINT `forum_forum_last_post_id_81b59e37_fk_forum_conversation_post_id` FOREIGN KEY (`last_post_id`) REFERENCES `forum_conversation_post` (`id`),
  CONSTRAINT `forum_forum_parent_id_22edea05_fk_forum_forum_id` FOREIGN KEY (`parent_id`) REFERENCES `forum_forum` (`id`),
  CONSTRAINT `forum_forum_chk_1` CHECK ((`type` >= 0)),
  CONSTRAINT `forum_forum_chk_10` CHECK ((`direct_topics_count` >= 0)),
  CONSTRAINT `forum_forum_chk_4` CHECK ((`link_redirects_count` >= 0)),
  CONSTRAINT `forum_forum_chk_5` CHECK ((`lft` >= 0)),
  CONSTRAINT `forum_forum_chk_6` CHECK ((`rght` >= 0)),
  CONSTRAINT `forum_forum_chk_7` CHECK ((`tree_id` >= 0)),
  CONSTRAINT `forum_forum_chk_8` CHECK ((`level` >= 0)),
  CONSTRAINT `forum_forum_chk_9` CHECK ((`direct_posts_count` >= 0))
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forum_forum`
--

LOCK TABLES `forum_forum` WRITE;
/*!40000 ALTER TABLE `forum_forum` DISABLE KEYS */;
INSERT INTO `forum_forum` VALUES (1,'2020-11-28 07:38:29.769965','2020-11-30 04:45:50.467093','Ansiedad','ansiedad','','',NULL,0,0,0,'2020-11-29 03:53:55.696961',1,'<p></p>',2,3,2,1,2,3,1,3),(2,'2020-11-28 07:46:29.341881','2020-11-28 07:46:29.341920','Salud mental','salud-mental','','',NULL,0,1,0,NULL,1,'<p></p>',1,28,2,0,NULL,0,0,NULL),(3,'2020-11-28 07:47:19.908547','2020-11-28 07:47:19.908602','Depresión','depresión','','',NULL,0,0,0,NULL,1,'<p></p>',4,5,2,1,2,0,0,NULL),(4,'2020-11-28 07:47:35.668611','2020-11-28 07:47:35.668650','Pensamientos suicidas','pensamientos-suicidas','','',NULL,0,0,0,NULL,1,'<p></p>',6,7,2,1,2,0,0,NULL),(5,'2020-11-28 07:47:48.551318','2020-11-28 07:47:48.551399','Transtorno bipolar','transtorno-bipolar','','',NULL,0,0,0,NULL,1,'<p></p>',8,9,2,1,2,0,0,NULL),(6,'2020-11-28 07:47:59.257342','2020-11-28 07:47:59.257382','Deficit de atención','deficit-de-atención','','',NULL,0,0,0,NULL,1,'<p></p>',10,11,2,1,2,0,0,NULL),(7,'2020-11-28 07:48:16.029858','2020-11-28 07:48:16.029899','Esquizofrenia','esquizofrenia','','',NULL,0,0,0,NULL,1,'<p></p>',12,13,2,1,2,0,0,NULL),(8,'2020-11-28 07:48:27.223210','2020-11-28 07:48:27.223276','Anorexia','anorexia','','',NULL,0,0,0,NULL,1,'<p></p>',14,15,2,1,2,0,0,NULL),(9,'2020-11-28 07:48:39.588832','2020-11-28 07:48:39.588894','Anorexia','anorexia','','',NULL,0,0,0,NULL,1,'<p></p>',16,17,2,1,2,0,0,NULL),(10,'2020-11-28 07:49:07.800708','2020-11-28 07:49:07.800746','Bulimia','bulimia','','',NULL,0,0,0,NULL,1,'<p></p>',18,19,2,1,2,0,0,NULL),(11,'2020-11-28 07:49:47.954672','2020-11-28 07:49:47.954760','Trastorno obsesivo-compulsivo','trastorno-obsesivo-compulsivo','','',NULL,0,0,0,NULL,1,'<p></p>',20,21,2,1,2,0,0,NULL),(12,'2020-11-28 07:50:18.721443','2020-11-28 07:50:18.721505','Trastorno de pánico','trastorno-de-pánico','','',NULL,0,0,0,NULL,1,'<p></p>',22,23,2,1,2,0,0,NULL),(13,'2020-11-28 07:50:49.778818','2020-11-28 07:50:49.778871','Insomnio','insomnio','','',NULL,0,0,0,NULL,1,'<p></p>',24,25,2,1,2,0,0,NULL),(14,'2020-11-28 07:51:03.632473','2020-11-28 07:51:03.632537','Autoestima','autoestima','','',NULL,0,0,0,NULL,1,'<p></p>',26,27,2,1,2,0,0,NULL);
/*!40000 ALTER TABLE `forum_forum` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `forum_member_forumprofile`
--

DROP TABLE IF EXISTS `forum_member_forumprofile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `forum_member_forumprofile` (
  `id` int NOT NULL AUTO_INCREMENT,
  `avatar` varchar(100) DEFAULT NULL,
  `signature` longtext,
  `posts_count` int unsigned NOT NULL,
  `_signature_rendered` longtext,
  `user_id` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`),
  CONSTRAINT `forum_member_forumprofile_user_id_9d6b9b6b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `forum_member_forumprofile_chk_1` CHECK ((`posts_count` >= 0))
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forum_member_forumprofile`
--

LOCK TABLES `forum_member_forumprofile` WRITE;
/*!40000 ALTER TABLE `forum_member_forumprofile` DISABLE KEYS */;
INSERT INTO `forum_member_forumprofile` VALUES (1,'machina/avatar_images/a6001ac56b9a45c8ac50d225a33cb764.png','',1,'<p></p>',1),(2,'machina/avatar_images/641e646295cd420492226275dfa8f251.png','',1,'<p></p>',3),(3,'',NULL,0,NULL,2),(4,'',NULL,1,NULL,4);
/*!40000 ALTER TABLE `forum_member_forumprofile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `forum_permission_forumpermission`
--

DROP TABLE IF EXISTS `forum_permission_forumpermission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `forum_permission_forumpermission` (
  `id` int NOT NULL AUTO_INCREMENT,
  `codename` varchar(150) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `codename` (`codename`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forum_permission_forumpermission`
--

LOCK TABLES `forum_permission_forumpermission` WRITE;
/*!40000 ALTER TABLE `forum_permission_forumpermission` DISABLE KEYS */;
INSERT INTO `forum_permission_forumpermission` VALUES (18,'can_approve_posts'),(12,'can_attach_file'),(10,'can_create_polls'),(7,'can_delete_own_posts'),(17,'can_delete_posts'),(13,'can_download_file'),(8,'can_edit_own_posts'),(16,'can_edit_posts'),(14,'can_lock_topics'),(15,'can_move_topics'),(5,'can_post_announcements'),(6,'can_post_stickies'),(9,'can_post_without_approval'),(2,'can_read_forum'),(19,'can_reply_to_locked_topics'),(4,'can_reply_to_topics'),(1,'can_see_forum'),(3,'can_start_new_topics'),(11,'can_vote_in_polls');
/*!40000 ALTER TABLE `forum_permission_forumpermission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `forum_permission_groupforumpermission`
--

DROP TABLE IF EXISTS `forum_permission_groupforumpermission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `forum_permission_groupforumpermission` (
  `id` int NOT NULL AUTO_INCREMENT,
  `has_perm` tinyint(1) NOT NULL,
  `forum_id` int DEFAULT NULL,
  `group_id` int NOT NULL,
  `permission_id` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `forum_permission_groupfo_permission_id_forum_id_g_a1e477c8_uniq` (`permission_id`,`forum_id`,`group_id`),
  KEY `forum_permission_gro_forum_id_d59d5cac_fk_forum_for` (`forum_id`),
  KEY `forum_permission_gro_group_id_b515635b_fk_auth_grou` (`group_id`),
  KEY `forum_permission_groupforumpermission_has_perm_48cae01d` (`has_perm`),
  CONSTRAINT `forum_permission_gro_forum_id_d59d5cac_fk_forum_for` FOREIGN KEY (`forum_id`) REFERENCES `forum_forum` (`id`),
  CONSTRAINT `forum_permission_gro_group_id_b515635b_fk_auth_grou` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `forum_permission_gro_permission_id_2475fe70_fk_forum_per` FOREIGN KEY (`permission_id`) REFERENCES `forum_permission_forumpermission` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forum_permission_groupforumpermission`
--

LOCK TABLES `forum_permission_groupforumpermission` WRITE;
/*!40000 ALTER TABLE `forum_permission_groupforumpermission` DISABLE KEYS */;
/*!40000 ALTER TABLE `forum_permission_groupforumpermission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `forum_permission_userforumpermission`
--

DROP TABLE IF EXISTS `forum_permission_userforumpermission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `forum_permission_userforumpermission` (
  `id` int NOT NULL AUTO_INCREMENT,
  `has_perm` tinyint(1) NOT NULL,
  `anonymous_user` tinyint(1) NOT NULL,
  `forum_id` int DEFAULT NULL,
  `permission_id` int NOT NULL,
  `user_id` int DEFAULT NULL,
  `authenticated_user` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `forum_permission_userfor_permission_id_forum_id_u_024a3693_uniq` (`permission_id`,`forum_id`,`user_id`),
  KEY `forum_permission_use_forum_id_f781f4d6_fk_forum_for` (`forum_id`),
  KEY `forum_permission_use_user_id_8106d02d_fk_auth_user` (`user_id`),
  KEY `forum_permission_userforumpermission_anonymous_user_8aabbd9d` (`anonymous_user`),
  KEY `forum_permission_userforumpermission_has_perm_1b5ee7ac` (`has_perm`),
  KEY `forum_permission_userforumpermission_authenticated_user_b677afa4` (`authenticated_user`),
  CONSTRAINT `forum_permission_use_forum_id_f781f4d6_fk_forum_for` FOREIGN KEY (`forum_id`) REFERENCES `forum_forum` (`id`),
  CONSTRAINT `forum_permission_use_permission_id_9090e930_fk_forum_per` FOREIGN KEY (`permission_id`) REFERENCES `forum_permission_forumpermission` (`id`),
  CONSTRAINT `forum_permission_use_user_id_8106d02d_fk_auth_user` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forum_permission_userforumpermission`
--

LOCK TABLES `forum_permission_userforumpermission` WRITE;
/*!40000 ALTER TABLE `forum_permission_userforumpermission` DISABLE KEYS */;
/*!40000 ALTER TABLE `forum_permission_userforumpermission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `forum_polls_topicpoll`
--

DROP TABLE IF EXISTS `forum_polls_topicpoll`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `forum_polls_topicpoll` (
  `id` int NOT NULL AUTO_INCREMENT,
  `created` datetime(6) NOT NULL,
  `updated` datetime(6) NOT NULL,
  `question` varchar(255) NOT NULL,
  `duration` int unsigned DEFAULT NULL,
  `max_options` smallint unsigned NOT NULL,
  `user_changes` tinyint(1) NOT NULL,
  `topic_id` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `topic_id` (`topic_id`),
  CONSTRAINT `forum_polls_topicpol_topic_id_1b827b83_fk_forum_con` FOREIGN KEY (`topic_id`) REFERENCES `forum_conversation_topic` (`id`),
  CONSTRAINT `forum_polls_topicpoll_chk_1` CHECK ((`duration` >= 0)),
  CONSTRAINT `forum_polls_topicpoll_chk_2` CHECK ((`max_options` >= 0))
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forum_polls_topicpoll`
--

LOCK TABLES `forum_polls_topicpoll` WRITE;
/*!40000 ALTER TABLE `forum_polls_topicpoll` DISABLE KEYS */;
INSERT INTO `forum_polls_topicpoll` VALUES (1,'2020-11-28 07:44:19.247380','2020-11-29 03:23:45.822854','',0,1,0,1);
/*!40000 ALTER TABLE `forum_polls_topicpoll` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `forum_polls_topicpolloption`
--

DROP TABLE IF EXISTS `forum_polls_topicpolloption`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `forum_polls_topicpolloption` (
  `id` int NOT NULL AUTO_INCREMENT,
  `text` varchar(255) NOT NULL,
  `poll_id` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `forum_polls_topicpol_poll_id_a54cbd58_fk_forum_pol` (`poll_id`),
  CONSTRAINT `forum_polls_topicpol_poll_id_a54cbd58_fk_forum_pol` FOREIGN KEY (`poll_id`) REFERENCES `forum_polls_topicpoll` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forum_polls_topicpolloption`
--

LOCK TABLES `forum_polls_topicpolloption` WRITE;
/*!40000 ALTER TABLE `forum_polls_topicpolloption` DISABLE KEYS */;
/*!40000 ALTER TABLE `forum_polls_topicpolloption` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `forum_polls_topicpollvote`
--

DROP TABLE IF EXISTS `forum_polls_topicpollvote`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `forum_polls_topicpollvote` (
  `id` int NOT NULL AUTO_INCREMENT,
  `timestamp` datetime(6) NOT NULL,
  `poll_option_id` int NOT NULL,
  `voter_id` int DEFAULT NULL,
  `anonymous_key` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `forum_polls_topicpol_poll_option_id_a075b665_fk_forum_pol` (`poll_option_id`),
  KEY `forum_polls_topicpollvote_voter_id_0a287559_fk_auth_user_id` (`voter_id`),
  CONSTRAINT `forum_polls_topicpol_poll_option_id_a075b665_fk_forum_pol` FOREIGN KEY (`poll_option_id`) REFERENCES `forum_polls_topicpolloption` (`id`),
  CONSTRAINT `forum_polls_topicpollvote_voter_id_0a287559_fk_auth_user_id` FOREIGN KEY (`voter_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forum_polls_topicpollvote`
--

LOCK TABLES `forum_polls_topicpollvote` WRITE;
/*!40000 ALTER TABLE `forum_polls_topicpollvote` DISABLE KEYS */;
/*!40000 ALTER TABLE `forum_polls_topicpollvote` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `forum_tracking_forumreadtrack`
--

DROP TABLE IF EXISTS `forum_tracking_forumreadtrack`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `forum_tracking_forumreadtrack` (
  `id` int NOT NULL AUTO_INCREMENT,
  `mark_time` datetime(6) NOT NULL,
  `forum_id` int NOT NULL,
  `user_id` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `forum_tracking_forumreadtrack_user_id_forum_id_3e64777a_uniq` (`user_id`,`forum_id`),
  KEY `forum_tracking_forum_forum_id_bbd3fb47_fk_forum_for` (`forum_id`),
  KEY `forum_tracking_forumreadtrack_mark_time_72eec39e` (`mark_time`),
  CONSTRAINT `forum_tracking_forum_forum_id_bbd3fb47_fk_forum_for` FOREIGN KEY (`forum_id`) REFERENCES `forum_forum` (`id`),
  CONSTRAINT `forum_tracking_forumreadtrack_user_id_932d4605_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forum_tracking_forumreadtrack`
--

LOCK TABLES `forum_tracking_forumreadtrack` WRITE;
/*!40000 ALTER TABLE `forum_tracking_forumreadtrack` DISABLE KEYS */;
INSERT INTO `forum_tracking_forumreadtrack` VALUES (1,'2020-11-28 07:44:20.030272',1,1),(2,'2020-11-29 03:24:07.014382',1,3),(3,'2020-11-29 03:24:07.145661',2,3),(4,'2020-11-29 03:43:28.853090',1,2),(5,'2020-11-29 03:43:29.153364',2,2),(6,'2020-11-29 03:53:56.700851',1,4),(7,'2020-11-29 03:53:56.831846',2,4);
/*!40000 ALTER TABLE `forum_tracking_forumreadtrack` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `forum_tracking_topicreadtrack`
--

DROP TABLE IF EXISTS `forum_tracking_topicreadtrack`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `forum_tracking_topicreadtrack` (
  `id` int NOT NULL AUTO_INCREMENT,
  `mark_time` datetime(6) NOT NULL,
  `topic_id` int NOT NULL,
  `user_id` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `forum_tracking_topicreadtrack_user_id_topic_id_6fe3e105_uniq` (`user_id`,`topic_id`),
  KEY `forum_tracking_topic_topic_id_9a53bd45_fk_forum_con` (`topic_id`),
  KEY `forum_tracking_topicreadtrack_mark_time_7dafc483` (`mark_time`),
  CONSTRAINT `forum_tracking_topic_topic_id_9a53bd45_fk_forum_con` FOREIGN KEY (`topic_id`) REFERENCES `forum_conversation_topic` (`id`),
  CONSTRAINT `forum_tracking_topicreadtrack_user_id_2762562b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `forum_tracking_topicreadtrack`
--

LOCK TABLES `forum_tracking_topicreadtrack` WRITE;
/*!40000 ALTER TABLE `forum_tracking_topicreadtrack` DISABLE KEYS */;
/*!40000 ALTER TABLE `forum_tracking_topicreadtrack` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-11-29 21:51:09
